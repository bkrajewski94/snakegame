import React from 'react';
import styles from './Apple.module.css';
import appleImage from '../../assets/apple.PNG';

const apple = props => {
  return (
    <div
      className={styles.Apple}
      style={{
        height: props.boxsize,
        width: props.boxsize,
        left: props.apple.left,
        top: props.apple.top,
        backgroundImage: 'url(' + appleImage + ')'
      }}
    />
  );
};

export default apple;

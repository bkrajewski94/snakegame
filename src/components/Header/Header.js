import React from 'react';
import styles from './Header.module.css';

const header = props => (
  <div className={styles.Header}>
    <h1 className={styles.score}>Score: {props.snakeLenght}</h1>
  </div>
);

export default header;

import React from 'react';
import styles from './GameField.module.css';
import Board from '../../containers/Board/Board';
import StartPage from '../../containers/StartPage/StartPage';
import Summary from '../../containers/Summary/Summary';
import { Route } from 'react-router-dom';

const gameField = props => {
  return (
    <div className={styles.GameField}>
      <Route
        path="/"
        exact
        render={() => <StartPage resetScoreHandler={props.resetScoreHandler} />}
      />
      <Route
        path="/snake"
        render={() => (
          <Board
            snakeLenght={props.snakeLenght}
            icreaseLenght={props.icreaseLenght}
          />
        )}
      />
      <Route
        path="/scorelist"
        exact
        render={() => <Summary snakeLenght={props.snakeLenght} />}
      />
    </div>
  );
};

export default gameField;

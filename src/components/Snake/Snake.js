import React from 'react';
import styles from './Snake.module.css';
import headUp from '../../assets/headUp.png';
import headDown from '../../assets/headDown.png';
import headRight from '../../assets/headRight.png';
import headLeft from '../../assets/headLeft.png';
import tailUp from '../../assets/tailUp.png';
import tailDown from '../../assets/tailDown.png';
import tailLeft from '../../assets/tailLeft.png';
import tailRight from '../../assets/tailRight.png';
import rightDown from '../../assets/rightDown.png';
import rightUp from '../../assets/rightUp.png';
import leftUp from '../../assets/leftUp.png';
import leftDown from '../../assets/leftDown.png';
import horisontal from '../../assets/bodyhorizontally.png';
import vertical from '../../assets/bodyvertically.png';

let bgImage = headDown;

class Snake extends React.Component {
  render() {
    return (
      <>
        {this.props.snake.map((block, index) => {
          //head image
          if (index === 0) {
            const nextBlock = this.props.snake[index + 1];
            if (block.top < nextBlock.top) {
              bgImage = headUp;
            } else if (block.left > nextBlock.left) {
              bgImage = headRight;
            } else if (block.top > nextBlock.top) {
              bgImage = headDown;
            } else if (block.left < nextBlock.left) {
              bgImage = headLeft;
            }
          } else if (index === this.props.snake.length - 1) {
            // Tail image
            const prevBlock = this.props.snake[index - 1];
            if (prevBlock.top < block.top) {
              bgImage = tailUp;
            } else if (prevBlock.left > block.left) {
              bgImage = tailRight;
            } else if (prevBlock.top > block.top) {
              bgImage = tailDown;
            } else if (prevBlock.left < block.left) {
              bgImage = tailLeft;
            }
          } else {
            // Body parts image
            const prevBlock = this.props.snake[index - 1];
            const nextBlock = this.props.snake[index + 1];
            if (
              (prevBlock.left < block.left && nextBlock.left > block.left) ||
              (nextBlock.left < block.left && prevBlock.left > block.left)
            ) {
              bgImage = horisontal;
            } else if (
              (prevBlock.left < block.left && nextBlock.top > block.top) ||
              (nextBlock.left < block.left && prevBlock.top > block.top)
            ) {
              bgImage = leftDown;
            } else if (
              (prevBlock.top < block.top && nextBlock.top > block.top) ||
              (nextBlock.top < block.top && prevBlock.top > block.top)
            ) {
              bgImage = vertical;
            } else if (
              (prevBlock.top < block.top && nextBlock.left < block.left) ||
              (nextBlock.top < block.top && prevBlock.left < block.left)
            ) {
              bgImage = leftUp;
            } else if (
              (prevBlock.left > block.left && nextBlock.top < block.top) ||
              (nextBlock.left > block.left && prevBlock.top < block.top)
            ) {
              bgImage = rightUp;
            } else if (
              (prevBlock.top > block.top && nextBlock.left > block.left) ||
              (nextBlock.top > block.top && prevBlock.left > block.left)
            ) {
              bgImage = rightDown;
            }
          }
          return (
            <div
              className={styles.snakeBlock}
              style={{
                height: this.props.boxsize,
                width: this.props.boxsize,
                left: block.left,
                top: block.top,
                backgroundImage: 'url(' + bgImage + ')'
              }}
              key={index}
            />
          );
        })}
      </>
    );
  }
}

export default Snake;

import React, { Component } from 'react';
import './App.css';
import Header from './components/Header/Header';
import GameField from './components/GameField/GameField';

class App extends Component {
  state = {
    snakeLenght: 1
  };

  increaseSnakeLenghtHandler = () => {
    this.setState(prevState => {
      return { snakeLenght: prevState.snakeLenght + 1 };
    });
  };

  resetScoreHandler = () => {
    this.setState({ snakeLenght: 1 });
  };

  render() {
    return (
      <div className="App">
        <Header snakeLenght={this.state.snakeLenght} />
        <GameField
          snakeLenght={this.state.snakeLenght}
          icreaseLenght={this.increaseSnakeLenghtHandler}
          resetScoreHandler={this.resetScoreHandler}
        />
      </div>
    );
  }
}

export default App;

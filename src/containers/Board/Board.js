import React from 'react';
import { withRouter } from 'react-router-dom';
import styles from './Board.module.css';
import Snake from '../../components/Snake/Snake';
import Apple from '../../components/Apple/Apple';

class Board extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      boxsize: 30,
      snake: [
        {
          left: 0,
          top: 30
        },
        {
          left: 0,
          top: 0
        }
      ],
      direction: 'stop',
      timeInterval: 300,
      apple: {
        top: 0,
        left: 60
      },
      ready: true,
      stop: true
    };
    this.baseState = this.state;
  }

  eatAppleHandler = newHead => {
    const apple = this.state.apple;
    if (newHead.left === apple.left && newHead.top === apple.top) {
      this.changeAppleLocation();
      let snakeLenght = this.props.snakeLenght;
      let snakeSpeed = this.state.timeInterval;
      if (snakeLenght % 5 === 0 && snakeSpeed > 51) {
        this.setState({
          timeInterval: snakeSpeed * 0.8
        });
        this.props.icreaseLenght();
      } else {
        this.props.icreaseLenght();
      }
      return true;
    } else {
      return false;
    }
  };

  collisionHandler = (newHead, snakeModel) => {
    if (
      newHead.left >= 600 ||
      newHead.left < 0 ||
      newHead.top < 0 ||
      newHead.top >= 600 ||
      snakeModel.filter(
        snakeBlock =>
          snakeBlock.left === newHead.left && snakeBlock.top === newHead.top
      ).length > 0
    ) {
      this.setState(
        {
          state: this.baseState
        },
        () => {
          this.props.history.replace('/scorelist');
        }
      );

      return true;
    } else {
      return false;
    }
  };

  snakeMovementHandler = props => {
    const snakeModel = [...this.state.snake];
    let snakeX = snakeModel[0].left;
    let snakeY = snakeModel[0].top;
    let newHead = { left: snakeX, top: snakeY };
    switch (props) {
      case 'up':
        newHead.top = newHead.top - this.state.boxsize;
        break;
      case 'down':
        newHead.top = newHead.top + this.state.boxsize;
        break;
      case 'right':
        newHead.left = newHead.left + this.state.boxsize;
        break;
      case 'left':
        newHead.left = newHead.left - this.state.boxsize;
        break;
      default:
    }
    let appleEaten = this.eatAppleHandler(newHead);
    if (!appleEaten) {
      snakeModel.pop();
    }
    if (!this.collisionHandler(newHead, snakeModel)) {
      snakeModel.unshift(newHead);
      this.setState({
        snake: snakeModel,
        ready: true
      });
    }
  };

  keyDownHandler = event => {
    switch (event.keyCode) {
      case 37:
        if (this.state.direction !== 'right' && this.state.ready) {
          this.setState({ direction: 'left', ready: false });
        }
        break;
      case 38:
        if (this.state.direction !== 'down' && this.state.ready) {
          this.setState({ direction: 'up', ready: false });
        }
        break;
      case 39:
        if (this.state.direction !== 'left' && this.state.ready) {
          this.setState({ direction: 'right', ready: false });
        }
        break;
      case 40:
        if (this.state.direction !== 'up' && this.state.ready) {
          this.setState({ direction: 'down', ready: false });
        }
        break;
      default:
    }
  };

  snakeMovementInterval = () => {
    if (!this.state.stop) {
      this.timeout = setTimeout(
        this.snakeMovementInterval,
        this.state.timeInterval
      );
      if (this.state.direction !== 'stop') {
        this.snakeMovementHandler(this.state.direction);
      }
    }
  };

  componentDidMount() {
    this.changeAppleLocation();
    document.addEventListener('keydown', this.keyDownHandler, false);
    this.setState(
      {
        stop: false
      },
      () => {
        this.timeout = setTimeout(
          this.snakeMovementInterval(),
          this.state.timeInterval
        );
      }
    );
  }

  componentWillUnmount() {
    clearTimeout(this.timeout);
    document.removeEventListener('keydown', this.keyDownHandler, false);
  }

  generateRandomNumber() {
    const NumbersHolder = Math.floor(Math.random() * 20) * 30;
    return NumbersHolder;
  }

  changeAppleLocation() {
    const snake = [...this.state.snake];
    const oldApple = { ...this.state.apple };
    let newApple = { left: 0, top: 0 };
    newApple.left = this.generateRandomNumber();
    newApple.top = this.generateRandomNumber();
    while (
      snake.filter(
        snakeBlock =>
          snakeBlock.left === newApple.left && snakeBlock.top === newApple.top
      ).length > 0 ||
      (newApple.left === oldApple.left && newApple.top === oldApple.top)
    ) {
      newApple.left = this.generateRandomNumber();
      newApple.top = this.generateRandomNumber();
    }
    this.setState({ apple: newApple });
  }

  render() {
    return (
      <div className={styles.Board}>
        <Snake boxsize={this.state.boxsize} snake={this.state.snake} />
        <Apple boxsize={this.state.boxsize} apple={this.state.apple} />
      </div>
    );
  }
}

export default withRouter(Board);

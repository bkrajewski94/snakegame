import React from 'react';
import styles from './AddYourScore.module.css';
import Cup from '../../../assets/cup.png';

const addYourScore = props => {
  return (
    <>
      <h1 className={styles.score}>Your score: {props.score}</h1>
      <img src={Cup} alt="Winners Trophy" height="350" width="350" />
      <form onSubmit={props.handleSubmit}>
        <label>
          <input
            className={styles.scoreInput}
            type="text"
            placeholder="Name"
            onChange={props.handleInputChange}
            maxLength="16"
          />
        </label>
        <input className={styles.submitButton} type="submit" value="Submit" />
      </form>
    </>
  );
};

export default addYourScore;

import React, { Component } from 'react';
import styles from './Summary.module.css';
import AddYourScore from './AddYourScore/AddYourScore';
import ScoreTable from './ScoreTable/ScoreTable';
import shortid from 'shortid';

class Summary extends Component {
  state = {
    TopScores: null,
    name: '',
    showResultsTable: false,
    currentElementIndex: null
  };

  componentWillMount() {
    localStorage.getItem('TopScores') &&
      this.setState({
        TopScores: JSON.parse(localStorage.getItem('TopScores'))
      });
  }

  AddScoreHandler = event => {
    event.preventDefault();
    let shortId = shortid.generate();
    let newScore = {
      name: this.state.name,
      points: this.props.snakeLenght,
      id: shortId
    };
    let newTopScores = [];
    if (this.state.TopScores !== null) {
      newTopScores = [...this.state.TopScores];
    }

    newTopScores.push(newScore);

    if (newTopScores.length > 1) {
      newTopScores.sort((a, b) =>
        a.points < b.points ? 1 : b.points < a.points ? -1 : 0
      );
      if (newTopScores.length > 10) {
        newTopScores.pop();
      }
    }

    let currentIndex = newTopScores.indexOf(newScore);

    localStorage.setItem('TopScores', JSON.stringify(newTopScores));
    this.setState({
      TopScores: newTopScores,
      showResultsTable: true,
      currentElementIndex: currentIndex
    });
  };

  handleInputChange = event => {
    this.setState({ name: event.target.value });
  };

  render() {
    let summaryDisplay = (
      <AddYourScore
        score={this.props.snakeLenght}
        handleInputChange={this.handleInputChange}
        handleSubmit={this.AddScoreHandler}
      />
    );

    if (this.state.showResultsTable) {
      summaryDisplay = (
        <ScoreTable
          results={this.state.TopScores}
          currentResult={this.state.currentElementIndex}
        />
      );
    }

    return <div className={styles.Summary}>{summaryDisplay}</div>;
  }
}

export default Summary;

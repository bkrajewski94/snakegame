import React from 'react';
import styles from './ScoreTable.module.css';
import TableLine from './TableLine/TableLine';
import { withRouter } from 'react-router-dom';

const scoreTable = props => {
  let resultsTable = props.results.map((score, index) => {
    if (index === props.currentResult) {
      return (
        <li className={styles.newElement} key={score.id}>
          <TableLine
            number={index + 1}
            name={score.name}
            result={score.points}
          />
        </li>
      );
    } else {
      return (
        <li key={score.id}>
          <TableLine
            number={index + 1}
            name={score.name}
            result={score.points}
          />
        </li>
      );
    }
  });

  resultsTable.unshift(
    <li key={'header'}>
      <TableLine number="No." name="Name" result="Result" />
    </li>
  );

  return (
    <>
      <h1>Top scores:</h1>
      <ul className={styles.list}>{resultsTable}</ul>
      <button
        className={styles.submitButton}
        onClick={() => props.history.replace('/')}
      >
        Submit
      </button>
    </>
  );
};

export default withRouter(scoreTable);

import React from 'react';
import styles from './TableLine.module.css';

const tableLine = props => {
  return (
    <div className={styles.line}>
      <div className={styles.number}>{props.number}</div>
      <div className={styles.name}>{props.name}</div>
      <div className={styles.score}>{props.result}</div>
    </div>
  );
};

export default tableLine;
